package com.example.examplecustomviewloadingindicator

import android.R.color
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import androidx.annotation.Nullable

class TestCanvas : View {
    var pacmanPaint: Paint? = null
    var eyeOfPacmanPaint: Paint? = null
    var numDot = 3
    var colorPacman = resources.getColor(color.holo_orange_dark)
    var colorEye = resources.getColor(color.holo_blue_bright)

    constructor(context: Context?) : super(context) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        if (context != null) {
            if (attrs != null) {
                intAttrs(context, attrs)
            }
        }
        init()
    }

    constructor(context: Context?, @Nullable attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        if (context != null) {
            if (attrs != null) {
                intAttrs(context, attrs)
            }
        }
        init()
    }

    constructor(
        context: Context?,
        @Nullable attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        if (context != null) {
            if (attrs != null) {
                intAttrs(context, attrs)
            }
        }
        init()
    }

    private fun init() {
        pacmanPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        pacmanPaint!!.setColor(colorPacman)
        eyeOfPacmanPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        eyeOfPacmanPaint!!.setColor(colorEye)
    }

    fun intAttrs(context: Context, attrs: AttributeSet) {
        val a = context.theme.obtainStyledAttributes(attrs, R.styleable.TestCanvas, 0, 0)
        try {
            numDot = a.getInteger(R.styleable.TestCanvas_numDot, numDot)
            colorPacman = a.getColor(
                R.styleable.TestCanvas_pacmanColor,
                resources.getColor(color.holo_orange_dark)
            )
            colorEye = a.getColor(
                R.styleable.TestCanvas_eyeColor,
                resources.getColor(color.black)
            )
        } finally {
            a.recycle()
        }
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        var square = 300
        val top = 100
        val left = 100
        var right = left + square
        var bottom = top + square
        if (canvas != null) {
            pacmanPaint?.let {
                canvas.drawArc(
                    left.toFloat(), top.toFloat(),
                    right.toFloat(), bottom.toFloat(), 30F, 300F, true, it
                )
            }
        }
        val cx = (left + 180).toFloat()
        val cy = (top + 70).toFloat()
        val radius = 25f
        canvas!!.drawCircle(cx, cy, radius, eyeOfPacmanPaint!!)
    }
}
